﻿using System;
using System.Collections.Generic;

namespace MovieRentalKata
{
    enum PriceCode
    {
        REGULAR,
        NEW_RELEASE,
        CHILDRENS
    }   

    abstract class Price
    {
        public abstract PriceCode PriceCode { get; }
        public abstract double GetCharge(int daysRented);

        public virtual int GetFrequentRentalPoints(int daysRented)
        {
            return 1;
        }
    }

    class ChildrenPrice : Price
    {
        public override PriceCode PriceCode 
        {
            get => PriceCode.CHILDRENS;
        }

        public override double GetCharge(int daysRented)
        {
            double result = 1.5;
            if (daysRented > 3)
                result += (daysRented - 3) * 1.5;

            return result;
        }
    }

    class NewReleasePrice : Price
    {
        public override PriceCode PriceCode
        {
            get => PriceCode.NEW_RELEASE;
        }
        public override double GetCharge(int daysRented)
        {
            return daysRented * 3;
        }

        public override int GetFrequentRentalPoints(int daysRented)
        {
            return (daysRented > 1) ? 2 : 1;
        }
    }

    class RegularPrice : Price
    {
        public override PriceCode PriceCode
        {
            get => PriceCode.REGULAR;
        }
        public override double GetCharge(int daysRented)
        {
            double result = 2;
            if (daysRented > 2)
                result += (daysRented - 2) * 1.5;

            return result;
        }
    }

    class Movie 
    {
        
        private String title;
        private Price Price;

        public Movie(String title, PriceCode priceCode) 
        {
            this.title = title;
            this.PriceCode = priceCode;
        }

        public PriceCode PriceCode 
        {
            get => Price.PriceCode;
            set 
            {
                //priceCode = value;
                switch(value)
                {
                    case PriceCode.CHILDRENS:
                        Price = new ChildrenPrice();
                        break;
                    case PriceCode.NEW_RELEASE:
                        Price = new NewReleasePrice();
                        break;
                    case PriceCode.REGULAR:
                        Price = new RegularPrice();
                        break;
                }
            }
        }

        public String Title 
        {
            get => title;
        }

        public double GetCharge(int daysRented)
        {
            return Price.GetCharge(daysRented);
        }

        public int GetFrequentRenterPoints(int daysRented)
        {
            //// Add frequent renter points
            //int result = 1;

            //// Add bonus for a two day new release rental
            //if ((this.PriceCode == PriceCode.NEW_RELEASE) && daysRented > 1)
            //    result += 1;
            //return result;

            return Price.GetFrequentRentalPoints(daysRented);
        }
    }

    class Rental 
    {
        private Movie movie;
        private int daysRented;

        public Rental(Movie movie, int daysRented) 
        {
            this.movie = movie;
            this.daysRented = daysRented;
        }

        public int DaysRented 
        {
            get => daysRented;
        }

        public Movie Movie 
        {
            get => movie;
        }

        public double Charge
        {
            get
            {
                return movie.GetCharge(daysRented);
            }
        }

        public int FrequentRenterPoints
        {
            get
            {
                return movie.GetFrequentRenterPoints(daysRented);
            }
        }
    }

    class Customer 
    {
        private String name;
        private List<Rental> rentals = new List<Rental>();

        public Customer(String name) 
        {
            this.name = name;
        }

        public void AddRental(Rental rental) 
        {
            rentals.Add(rental);
        }

        public String Name 
        {
            get => name;
        }

        public String Statement() 
        {
         
          

            String result = "Rental Record for " + Name + "\n";

            foreach (var rental in rentals)
            {
          

                // Show figures for this rental
                result += "\t" + rental.Movie.Title + "\t" + rental.Charge.ToString() + "\n";

              
            }

            // Add footer lines
            result += "Amount owed is " + TotalCharge.ToString() + "\n";
            result += "You earned " + TotalFrequentRenterPoints.ToString() + " frequent renter points";

            return result;
        }

        public String HTMLStatement()
        {



            String result = "<h1>Rental Record for <em>" + Name + "</em></h1>\n";


            result += "<ol>\n";
            foreach (var rental in rentals)
            {


                // Show figures for this rental
                result += "<li><em>" + rental.Movie.Title + "</em>:" + rental.Charge.ToString() + "</li>\n";


            }
            result += "</ol\n>";

            // Add footer lines
            result += "<p>";
            result += "Amount owed is <em>" + TotalCharge.ToString() + "</em>\n";
            result += "</p>";
            result += "<p>";  
            result += "You earned <em>" + TotalFrequentRenterPoints.ToString() + "</em> frequent renter points";
            result += "</p>";
            return result;
        }

        public double TotalCharge
        {
            get
            {
                double totalAmount = 0;
                foreach (var rental in rentals)
                {
                    totalAmount += rental.Charge;
                }

                return totalAmount;
            }
        }

        public int TotalFrequentRenterPoints
        {
            get
            {
                int frequentRenterPoints = 0;

                foreach (var rental in rentals)
                {
                    frequentRenterPoints += rental.FrequentRenterPoints;
                }

                return frequentRenterPoints;
            }
        }


    }


    class MainClass
    {
        public static void Main(string[] args)
        {
            var movie01 = new Movie("Captain Marvel", PriceCode.NEW_RELEASE);
            var movie02 = new Movie("Aladdin", PriceCode.CHILDRENS);
            var movie03 = new Movie("Back To The Future", PriceCode.REGULAR);

            var customer = new Customer("Roger Ebert");

            var rental01 = new Rental(movie01, 2);
            var rental02 = new Rental(movie02, 3);
            var rental03 = new Rental(movie03, 2);

            customer.AddRental(rental01);
            customer.AddRental(rental02);
            customer.AddRental(rental03);

            Console.WriteLine(customer.Statement());

        }
    }
}
